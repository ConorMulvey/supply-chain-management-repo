// In src/features/bomSlice.js
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getComponentsByDiagram, postComponent, deleteComponent, updateComponent } from "../api/ComponentsAPI"; // Adjust the import path accordingly

// Define the asynchronous thunk for posting a new component
export const addNewItem = createAsyncThunk(
  "bom/addNewItem",
  async (newItem, { rejectWithValue }) => {
    try {
      const response = await postComponent(newItem);
      return newItem; // Assuming the response includes the new item with an ID assigned by the database
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

// Asynchronous thunk for updating an item
export const putItem = createAsyncThunk(
    "bom/putItem",
    async (item, { rejectWithValue }) => {
      try {
        const response = await updateComponent(item);
        console.log(response)
        return response; // Assuming the response includes the new item with an ID assigned by the database
      } catch (error) {
        return rejectWithValue(error.response.data);
      }
    }
  );

// Asynchronous thunk for removing an item
export const removeItem = createAsyncThunk(
    "bom/removeItem",
    async (componentId, { rejectWithValue }) => {
        try {
            await deleteComponent(componentId);
            return componentId; // Make sure this matches the item ID format in your items array
        } catch (error) {
            console.log(error);
            return rejectWithValue(error.response.data);
        }
    }
);

// Asynchronous thunk action using Amplify's get function
export const fetchInitialItems = createAsyncThunk(
  "bom/fetchInitialItems",
  async () => {
    const response = await getComponentsByDiagram();
    const mappedResponse = response.map((item) => ({
        id: item.id,
        parents: item.parents,
        title: item.title,
        description: item.title,
        templateName: item.templateName,
        components: item.components,
        context: item,
      }))
    return mappedResponse; // Assuming getComponent returns the list directly
  }
);

// The initialState and bomSlice remain similar to the previous example
const initialState = {
  items: [],
  status: "idle", // 'idle', 'loading', 'succeeded', 'failed'
  error: null,
};

const bomSlice = createSlice({
  name: "bom",
  initialState,
  reducers: {
    addItem: (state, action) => {
        console.log(state,action)
        state.items.push(action.payload);
    },
    removeComponent: (state, action) => {
        state.items = state.items.filter(item => item.id !== action.payload.id);
    },
    updateItem: (state, action) => {
      const index = state.items.findIndex(
        (item) => item.id === action.payload.id
      );
      if (index !== -1) {
        state.items[index] = action.payload;
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchInitialItems.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchInitialItems.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.items = action.payload;
      })
      .addCase(fetchInitialItems.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      })
      // Handle addNewItem action states
      .addCase(addNewItem.pending, (state) => {
        state.addItemStatus = "loading";
      })
      .addCase(addNewItem.fulfilled, (state, action) => {
        state.addItemStatus = "succeeded";
        state.items.push(action.payload); // Assuming the payload includes the new item
      })
      .addCase(addNewItem.rejected, (state, action) => {
        state.addItemStatus = "failed";
        state.error = action.error.message;
      })
      .addCase(removeItem.pending, (state) => {
        // Optional: Add state management for loading
      })
      .addCase(removeItem.fulfilled, (state, action) => {
        const index = state.items.findIndex(item => item.id === action.payload);
        if (index !== -1) {
          state.items.splice(index, 1); // Remove the item from the array
        }
      })
      .addCase(removeItem.rejected, (state, action) => {
        // Optional: Handle error state
      });
  },
});

export const { addItem, updateItem, removeComponent } = bomSlice.actions;

export default bomSlice.reducer;