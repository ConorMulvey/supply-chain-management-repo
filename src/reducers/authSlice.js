import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    currentUser: null,
    loggedIn: false
};

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        logIn: (state, action) => {
            state.currentUser = action.payload;
            state.loggedIn = true
        },
        logOut: (state) => {
            state.currentUser = null;
            state.loggedIn = false
        }
    }
});

export const { logIn, logOut } = authSlice.actions;

export default authSlice.reducer;
