import { useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signIn, getCurrentUser } from "aws-amplify/auth";
import { logIn } from "../reducers/authSlice";

export default function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(true)
  const [errorMessage, setErrorMessage] = useState(false);
  const [openErrorSnack, setOpenErrorSnack] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenErrorSnack(false);
  };

  async function handleSubmit(event) {
    event.preventDefault();
    if ((email || password) === "") {
      setErrorMessage("Please enter a Email and Password");
      setOpenErrorSnack(true);
    } else {
      setLoading(true)
      const trimmedEmail = email.trim().replace(/\s+/g, "");
      try {
        await signIn({
          username: trimmedEmail,
          password,
        }).then((response) => {
          if (
            response.isSignedIn === true &&
            response.nextStep.signInStep === "DONE"
          ) {
            dispatch(logIn(trimmedEmail));
            navigate("/Home");
          } else if (
            response.isSignedIn === false &&
            response.nextStep.signInStep === "CONFIRM_SIGN_UP"
          ) {
            navigate("/Verify", {
              state: {
                username: trimmedEmail,
              },
            });
          }
        });
      } catch (error) {
        console.log("Caught error:", error);
        setErrorMessage(error.message);
        setOpenErrorSnack(true);
      }
      setLoading(false)
    }
  }

  async function currentAuthenticatedUser() { 
    setLoading(true)
    try {
      const { username, userId } = await getCurrentUser();
      if (username && userId) {
        dispatch(logIn(username));
        navigate("/Home");
      }
    } catch (error) {
      console.log(error);
    }
    setLoading(false)
  }

  useEffect(() => {
    currentAuthenticatedUser();
  }, []);

  return (
    <>
      {loading ? (
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          sx={{ minHeight: "100vh" }}
        >
          <Typography>Loading...</Typography>
        </Grid>
      ) : (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign In
            </Typography>
            <Box
              component="form"
              onSubmit={handleSubmit}
              noValidate
              sx={{ mt: 1 }}
            >
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={(e) => setEmail(e.target.value)}
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={(e) => setPassword(e.target.value)}
                autoComplete="current-password"
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item xs>
                  {/* <Link href="#" variant="body2">
                Forgot password?
              </Link> */}
                </Grid>
                <Grid item>
                  <Link href="/SignUp" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
            </Box>
            <Snackbar
              open={openErrorSnack}
              autoHideDuration={6000}
              onClose={handleClose}
            >
              <Alert
                onClose={handleClose}
                severity="error"
                variant="filled"
                sx={{ width: "100%" }}
              >
                {errorMessage}
              </Alert>
            </Snackbar>
          </Box>
        </Container>
      )}
    </>
  );
}
