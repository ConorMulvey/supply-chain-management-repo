import { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import { confirmSignUp } from 'aws-amplify/auth';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate,useLocation } from 'react-router-dom';
import { logIn } from "../reducers/authSlice";

export default function Verify() {
  const [verificationCode, setVerificationCode] = useState('');
  const [errorMessage, setErrorMessage] = useState(false);
  const [openErrorSnack, setOpenErrorSnack] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();

  console.log(location)

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenErrorSnack(false);
  };

  async function handleSubmit(event) {
    event.preventDefault();
    const username =  location.state.username;
    if (verificationCode === "") {
        setErrorMessage("Please enter a Verification code");
        setOpenErrorSnack(true);
    } else {
        try {
            await confirmSignUp({
              username: username,
              confirmationCode: verificationCode
            }).then((response) => {
              console.log(response)
              if (response.isSignUpComplete === true && response.nextStep.signUpStep === "DONE") {
                  dispatch(logIn(username));
                  navigate("/Home");
                }
            })
          } catch (error) {
            console.log('error confirming sign up', error);
          }
    }
  }

  return (
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
            Enter your verification code
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
        <TextField
              margin="normal"
              required
              fullWidth
              id="verify"
              label="Verification Code"
              name="verify"
              value={verificationCode}
              onChange={(e) => setVerificationCode(e.target.value)}
              autoFocus
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Verify
            </Button>
        </Box>
        <Snackbar
          open={openErrorSnack}
          autoHideDuration={6000}
          onClose={handleClose}
        >
          <Alert
            onClose={handleClose}
            severity="error"
            variant="filled"
            sx={{ width: "100%" }}
          >
            {errorMessage}
          </Alert>
        </Snackbar>
      </Box>
  );
}