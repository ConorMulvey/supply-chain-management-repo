import { useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useLocation,
  Navigate,
} from "react-router-dom";
import SignIn from "./page/SignIn";
import SignUp from "./page/SignUp";
import Verify from "./page/Verify";
import Home from "./page/Home";
import config from "./amplifyconfiguration.json";
import { Amplify } from "aws-amplify";
import { getCurrentUser } from "aws-amplify/auth";
import { useSelector, useDispatch } from "react-redux";
import { logIn } from "./reducers/authSlice";

Amplify.configure(config);

const ProtectedRoute = ({ children }) => {
  const location = useLocation();
  const isLoggedIn = useSelector((state) => state.auth.loggedIn);

  if (!isLoggedIn) {
    return <Navigate to="/" replace state={{ from: location }} />;
  }

  return children;
};

function App() {
  const dispatch = useDispatch();

  async function currentAuthenticatedUser() {
    try {
      const { username, userId } = await getCurrentUser();
      if (username && userId) {
        dispatch(logIn(username));
      }
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    currentAuthenticatedUser();
  }, []);

  return (
    <Router>
      <Routes>
        <Route path="/" element={<SignIn />} />
        <Route path="/SignUp" element={<SignUp />} />
        <Route path="/Verify" element={<Verify />} />
        <Route
          path="/Home"
          element={
            <ProtectedRoute>
              <Home />
            </ProtectedRoute>
          }
        />
      </Routes>
    </Router>
  );
}

export default App;