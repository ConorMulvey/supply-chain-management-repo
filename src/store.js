// src/store.js
import { configureStore } from '@reduxjs/toolkit';
import authReducer from "./reducers/authSlice";
import bomReducer from './reducers/bomSlice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    bom: bomReducer,
  },
});
