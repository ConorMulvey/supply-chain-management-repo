import { post, get, del, put } from "aws-amplify/api";

export const postComponent = async (data) => {
  try {
    const restOperation = post({
      apiName: "supplyChainApi",
      path: "/components",
      options: {
        body: data,
      },
    });
    
    const { body } = await restOperation.response;
    const response = await body.json();

    console.log(response)

    return response;
  } catch (error) {
    console.error("POST call failed: ", error);
    throw error;
  }
};

export const getComponent = async () => {
  try {
    const restOperation = get({
      apiName: "supplyChainApi",
      path: "/components",
    });

    const { body } = await restOperation.response;
    const response = await body.json();

    return response;
  } catch (error) {
    console.error("GET call failed: ", error);
    throw error;
  }
};

export const getComponentsByDiagram = async (data) => {
  try {
    const restOperation = get({
      apiName: "supplyChainApi",
      path: "/components",
      options: {
        queryParams: {
          diagram: data
        }
      }
    });

    const { body } = await restOperation.response;
    const response = await body.json();

    return response;
  } catch (error) {
    console.error("GET call failed: ", error);
    throw error;
  }
};

export const deleteComponent = async (data) => {
  console.log(data)
  try {
    const restOperation = del({
      apiName: 'supplyChainApi',
      path: '/components/object/'+data,
    });
    await restOperation.response;
    console.log('DELETE call succeeded');
  } catch (e) {
    console.log('DELETE call failed: ', JSON.parse(e.response.body));
  }
}

export const updateComponent = async (data) => {
  try {
    const restOperation = put({
      apiName: 'supplyChainApi',
      path: '/components/object/'+data.id,
      options: {
        body: data
      }
    });
    const response = await restOperation.response;
    console.log('PUT call succeeded: ', response);
  } catch (e) {
    console.log('PUT call failed: ', JSON.parse(e.response.body));
  }
}