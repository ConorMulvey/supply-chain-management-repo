import React, { useState, useEffect } from 'react';
import { FamDiagram } from 'basicprimitivesreact';
import { LCA, LevelAnnotationConfig, Enabled, Thickness, PageFitMode, Size, AnnotationType, Colors, LineType, HighlightPathAnnotationConfig } from 'basicprimitives';
import { Button, Box, Typography, Paper, Drawer  } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import ComponentCard from './ComponentCard';
import { getComponentsByDiagram } from '../api/ComponentsAPI';

const BomDiagram = ({ items, onNodeClick }) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [initialData, setInitialData] = useState(items);
  const [selectedNodeId, setSelectedNodeId] = useState(null);

  useEffect(() => {
    setInitialData(items);
    console.log(items)
  }, [items]);

  const handleDrawerOpen = () => {
    console.log('!!')
    setIsDrawerOpen(true);
  };

  // Function to handle closing the drawer
  const handleDrawerClose = () => {
    setIsDrawerOpen(false);
  };

  let components = initialData.map((node) => {
    return {
      id: node.id,
      parents: node.Parents,
      title: node.Title,
      description: node.Title,
      templateName: node.TemplateName,
      components: node.Components,
      context: node
    };
  });

  let config = {
    cursorItem: 0,
    enableMatrixLayout: true,
    minimumMatrixSize: 3,
    hasButtons: Enabled.True,
    buttonsPanelSize: 40,
    hasSelectorCheckbox: Enabled.False,
    normalItemsInterval: 40,
    pageFitMode: PageFitMode.None,
    annotations: [
      {
        annotationType: AnnotationType.Level,
        levels: [0],
        title: "Level 0",
        titleColor: Colors.RoyalBlue,
        offset: new Thickness(0, 0, 0, -1),
        lineWidth: new Thickness(0, 0, 0, 0),
        opacity: 0,
        borderColor: Colors.Gray,
        fillColor: Colors.Gray,
        lineType: LineType.Dotted
      },
      new LevelAnnotationConfig({
        levels: [1],
        title: "Level 1",
        titleColor: Colors.Green,
        offset: new Thickness(0, 0, 0, -1),
        lineWidth: new Thickness(0, 0, 0, 0),
        opacity: 0.08,
        borderColor: Colors.Gray,
        fillColor: Colors.Gray,
        lineType: LineType.Dotted
      }),
      new LevelAnnotationConfig({
        levels: [2],
        title: "Level 2",
        titleColor: Colors.Red,
        offset: new Thickness(0, 0, 0, -1),
        lineWidth: new Thickness(0, 0, 0, 0),
        opacity: 0,
        borderColor: Colors.Gray,
        fillColor: Colors.Gray,
        lineType: LineType.Solid
      })
    ],
    onButtonsRender: ({ context: itemConfig }) => {
      if (itemConfig.id === selectedNodeId) {
        return (
          <>
            <IconButton key="1" className="StyledButton">
              <AddIcon />
            </IconButton>
            <IconButton key="2" className="StyledButton">
              <DeleteIcon />
            </IconButton>
          </>
        );
      }
      return null;
    },
    // templates: [
    //   {
    //     name: "componentTemplate",
    //     itemSize: { width: 185, height: 140 },
    //     minimizedItemSize: new Size({ width: 14, height: 14 }),
    //     onItemRender: ({ context: itemConfig }) => {
    //       return (
    //         <ComponentCard title={itemConfig.title} description={itemConfig.description}></ComponentCard>
    //       );
    //     }
    //   }
    // ],
    items: components
  };

  return (
    <>
    <FamDiagram 
        config={config} 
        onButtonClick={(event, args) => {
        if (args.name === "add") {
          // Add functionality here to create a new node
        }
      }}
      onCursorChanged={(event, args) => {
        setSelectedNodeId(args.context.id);
      }}
      onItemClick={(event, args) => {
        setSelectedNodeId(args.context.id);
        onNodeClick(args.context);
      }}/>
    <Drawer anchor="right" open={isDrawerOpen} onClose={handleDrawerClose}>
        <div>This is the drawer content</div>
      </Drawer>
    </>
    )
}

export default BomDiagram;
