// File: BillOfMaterials.js
import React, { useState } from 'react';
import { OrgDiagram } from 'basicprimitivesreact';
import { Enabled, PageFitMode, GroupByType } from 'basicprimitives';

const BillOfMaterials = () => {
  const [nodes, setNodes] = useState([
    { id: 1, parents: [], title: "Root", description: "Root Node" }
  ]);
  const [nextId, setNextId] = useState(2);

  const addItem = (parentId) => {
    const newItem = {
      id: nextId,
      parents: [parentId],
      title: `Node ${nextId}`,
      description: `Node ${nextId} Description`
    };
    setNodes([...nodes, newItem]);
    setNextId(nextId + 1);
  };

  const removeItem = (id) => {
    setNodes(nodes.filter(node => node.id !== id && !node.parents.includes(id)));
  };

  return (
    <div>
      <button onClick={() => addItem(1)}>Add Node</button>
      <OrgDiagram
        centerOnCursor={true}
        config={{
          items: nodes.map(node => ({
            id: node.id,
            parents: node.parents,
            title: node.title,
            description: node.description,
          })),
          cursorItem: 0,
          hasSelectorCheckbox: Enabled.False,
          pageFitMode: PageFitMode.FitToPage,
          arrowsDirection: GroupByType.Parents,
          showExtraArrows: false,
          extraArrowsMinimumSpace: 30,
          isCursor: itemConfig => itemConfig.id === 1,
        }}
      />
      {/* This button is just for demonstration; use a form or specific logic to choose which node to remove */}
      <button onClick={() => removeItem(2)}>Remove Node</button>
    </div>
  );
};

export default BillOfMaterials;
