import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button } from '@mui/material';

const AddComponentDialog = ({ open, onClose, onSave }) => {
  const [newComponent, setNewComponent] = useState({
    part: '',
    manufacturingSite: '',
    costPerUnit: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewComponent({ ...newComponent, [name]: value });
  };

  const handleSave = () => {
    onSave(newComponent);
    onClose(); // Close dialog after saving
    // Reset form
    setNewComponent({ part: '', manufacturingSite: '', costPerUnit: '' });
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Add New Component</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          name="part"
          label="Name"
          type="text"
          fullWidth
          variant="outlined"
          value={newComponent.part}
          onChange={handleChange}
        />
        <TextField
          margin="dense"
          name="manufacturingSite"
          label="Manufacturing Site"
          type="text"
          fullWidth
          variant="outlined"
          value={newComponent.manufacturingSite}
          onChange={handleChange}
        />
        <TextField
          margin="dense"
          name="costPerUnit"
          label="Cost per Unit"
          type="number"
          fullWidth
          variant="outlined"
          value={newComponent.costPerUnit}
          onChange={handleChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={handleSave}>Save</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddComponentDialog;