// In src/components/BillOfMaterialsDiagram.jsx
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FamDiagram } from "basicprimitivesreact";
import {
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Drawer,
  TextField,
  OutlinedInput, 
  Chip
} from "@mui/material";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import { fetchInitialItems, addNewItem, removeItem,updateItem, removeComponent, addItem } from "../reducers/bomSlice";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

const BillOfMaterialsDiagram = () => {
  const dispatch = useDispatch();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [newItem, setNewItem] = useState({ title: "", parents: [] });
  const [mode, setMode] = useState('add'); // 'add' or 'edit'
  const [selectedItem, setSelectedItem] = useState(null);
  const { items, status, error } = useSelector((state) => state.bom);
  const [config, setConfig] = useState({
    items: useSelector((state) => state.bom.items),
    cursorItem: 0,
    pageFitMode: 1,
    minimalVisibility: 2,
    orientationType: 0,
    verticalAlignment: 1,
    horizontalAlignment: 0,
    linesWidth: 1,
    linesColor: "black",
    hasSelectorCheckbox: true,
    arrowsDirection: 2,
    showExtraArrows: false,
    itemsSize: { width: 120, height: 100 },
    hasButtons: true,
    buttonsPanelSize: 40,
    onButtonsRender: ({ context: itemConfig }) => {
      return (
        <>
          <IconButton onClick={() => openDrawerToAddChild(itemConfig.id)}>
            <AddIcon />
          </IconButton>
          <IconButton onClick={() => openDrawerToEdit(itemConfig.context)}>
            <EditIcon />
          </IconButton>
          <IconButton onClick={() => handleDeleteItem(itemConfig.id)}>
            <DeleteIcon />
          </IconButton>
        </>
      );
    },
  });

  useEffect(() => {
    if (status === "idle") {
      dispatch(fetchInitialItems()).unwrap()
      .then((components) => {
        console.log("Item recieved");
        setConfig((prevConfig) => ({
          ...prevConfig,
          items: components
        }));
      })
    }
  }, [status, dispatch]);

  useEffect(() => {
    setConfig((prevConfig) => ({
      ...prevConfig,
      items: items.map((item) => ({
        id: item.id,
        parents: item.parents,
        title: item.title,
        description: item.title,
        templateName: item.templateName,
        components: item.components,
        context: item,
      })),
    }));
  }, [items]);

  function findItemById(items, id) {
    return items.find(item => item.id === id);
  }

  const handleParentChange = (event) => {
    const {
      target: { value },
    } = event;
    setNewItem((prevItem) => ({
      ...prevItem,
      parents: typeof value === 'string' ? value.split(',') : value,
    }));
  };
  
  const openDrawerToEdit = (item) => {
    setMode('edit');
    setNewItem({ title: item.title, parents: item.parents });
    setSelectedItem(item);
    setDrawerOpen(true);
  };
  
  const handleDeleteItem = (itemId) => {
    console.log(items)
    dispatch(removeItem(itemId))
      .unwrap()
      .then(() => {
        console.log("Item removed successfully");
        const foundItem = findItemById(items, itemId);
        dispatch(removeComponent(foundItem))
        // setConfig((prevConfig) => ({
        //   ...prevConfig,
        //   items: items.filter(function (item) {
        //     return item !== foundItem;
        //   })
        // }))
        console.log(config)
      })
      .catch((error) => {
        console.error("Failed to remove item:", error);
        // Handle deletion error, e.g., displaying an error message
      });
  };

  const openDrawerToAddChild = (parentId) => {
    setNewItem({ title: "", parents: [parentId] }); // Set the parent ID
    setDrawerOpen(true); // Open the drawer
  };

  const handleAddNewNodeWithoutParent = () => {
    setNewItem({ title: "", parents: [] }); // Reset or initialize the form state
    setMode('add'); // Assuming you are using a mode state to toggle between add and edit
    setDrawerOpen(true); // Open the drawer
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "Parents") {
      setNewItem((prevItem) => ({
        ...prevItem,
        [name]: typeof value === "string" ? value.split(",") : value,
      }));
    } else {
      setNewItem((prevItem) => ({
        ...prevItem,
        [name]: value,
      }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  
    if (mode === 'add') {
      const newItemWithId = { ...newItem, id: items.length ? (Math.max(...items.map(item => item.id)) + 1).toString() : "1" };
      dispatch(addNewItem(newItemWithId))
        .unwrap()
        .then(() => 
          console.log(newItemWithId),
          //dispatch(addItem(newItemWithId)),
          // setConfig((prevConfig) => ({
          //   ...prevConfig,
          //   items: items.push(newItemWithId)
          // })),
          )
        .catch((error) => console.error("Failed to add new item:", error));
    } else if (mode === 'edit' && selectedItem) {
      const updatedItem = {
        ...selectedItem,
        ...newItem, 
      };
      dispatch(updateItem(updatedItem))
        .unwrap()
        .then(() => console.log("Item updated"))
        .catch((error) => console.error("Failed to update item:", error));
    }
  
    setDrawerOpen(false);
  };

  return (
    <>
    <Box>
      <Button
        variant="contained"
        onClick={handleAddNewNodeWithoutParent}
        sx={{ ml: 2, height: 'fit-content' }}
      >
        Add New Node
      </Button>
    </Box>
      <FamDiagram centerOnCursor config={config} key={config}/>
      <Drawer
        anchor="right"
        open={drawerOpen}
        onClose={() => setDrawerOpen(false)}
      >
        <Box sx={{ width: 250, padding: 2, mt: 10 }} component="form" onSubmit={handleSubmit}>
          <FormControl fullWidth>
            <TextField
              label="Title"
              variant="outlined"
              value={newItem.title}
              onChange={(e) =>
                setNewItem({ ...newItem, title: e.target.value })
              }
              margin="normal"
            />
            <FormControl fullWidth sx={{ mt: 2 }}>
              <InputLabel id="parents-select-label">Parents</InputLabel>
              <Select
                labelId="parents-select-label"
                id="parents-select"
                multiple
                value={newItem.parents}
                onChange={handleParentChange}
                input={<OutlinedInput id="select-multiple-chip" label="Parents" />}
                renderValue={(selected) => (
                  <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={items.find(item => item.id === value)?.title || value} />
                    ))}
                  </Box>
                )}
              >
                {items.map((item) => (
                  <MenuItem
                    key={item.id}
                    value={item.id}
                  >
                    {item.title}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Button
              variant="contained"
              type="submit"
              onClick={() => {
                console.log("Submit New Item", newItem);
                setDrawerOpen(false);
              }}
            >
              Add Item
            </Button>
          </FormControl>
        </Box>
      </Drawer>
    </>
  );
};

export default BillOfMaterialsDiagram;
